import {
    ADD_COUNTER,
    ADD_TO_CART,
}from './mutations-types'

export default{
     // 保存y轴方法
     SetSavey(state, item) {
        state.saveY = item;
      },
  
      [ADD_COUNTER](state,payload){
        payload.count++
      },
      [ADD_TO_CART](state,payload){
  
        state.cartlist.push(payload)
      }
  
  
}