import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'
Vue.use(Vuex)

const state = {
  return: {
    // Y轴定位
    saveY: 0,
  },
  // 购物车列表
  cartlist: [],
}

export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions,
  modules: {

  }

})