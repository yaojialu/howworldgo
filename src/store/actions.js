import {
    ADD_COUNTER,
    ADD_TO_CART,
}from './mutations-types'

export default{
         // 购物车添加
         addtocart({state,commit}, payload) {
            console.log(payload)
            // 查找之前判断数组中是否有这个商品
              let Oldproduct = state.cartlist.find(item=>item.iid===payload.iid)
      
            // 判断是否有Oldproduct
            if (Oldproduct) {
              commit(ADD_COUNTER,Oldproduct)
            } else {
              payload.count = 1
              commit(ADD_TO_CART,payload)
            } 
          },
}