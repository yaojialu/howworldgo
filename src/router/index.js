import Vue from 'vue'
import VueRouter from 'vue-router'


// const originalPush = Router.prototype.push;
// Router.prototype.push = function push(location, onResolve, onReject) {
//     if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject);
//     return originalPush.call(this, location).catch(err => err);
// };




Vue.use(VueRouter)

const index = () => import('../views/Home/index.vue')
const cart = () => import('../views/cart/cart.vue')
const category = () => import('../views/category/category.vue')
const profiles = () => import('../views/profiles/profiles.vue')
const detail = () => import('../views/detail/detail.vue')

const login = () => import('../views/login/login.vue')


const routes = [
 // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },

{
  path:'/',
  redirect:'/index',
  // name:'index'
  meta:{
    keepAlive:true //需要被缓存的组件
 },
},
  
  // 主页路径
  {
    path:'/index',
    name:'index',
    component:index,
    meta:{
      title:'首页',
      keepAlive:true
    }
  },
    // cart路径
  {
    path:'/cart',
    name:'cart',
    component:cart,
    meta:{
      title:'分类',
      keepAlive:true
    }
  },
      // category路径
  {
    path:'/category',
    name:'category',
    component:category,
    meta:{
      title:'购物车',
      keepAlive:true
    }
  },
        // profiles路径
  {
    path:'/profiles',
    name:'profiles',
    component:profiles,
    meta:{
      title:'我的',
      keepAlive:true
    }
  },

  //详情
  {
    path:'/detail/:iid',
    component:detail,
    meta:{
      title:'商品详情页面',
      keepAlive:true
    }
  },
  // 
  {
    path:'/detail/:item_id',
    component:detail,
    meta:{
      title:'商品详情页面',
      keepAlive:true
    }
  },
  // 登录
  {
    path:'/login',
    name:'login',
    component:login,
    meta:{
      title:'登录'
    }
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
