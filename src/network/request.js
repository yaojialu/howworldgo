import axios from 'axios'

export function request(config){
    // 设定baseurl 操作延时
    const instance =axios.create({
        baseURL:'http://152.136.185.210:7878/api/hy66',
        timeout:6000,
    })
    // 拦截器
    instance.interceptors.request.use(config=>{
    // 记得返回config
        return config
    },err=>{
        console.log(err);
    }) 
    return instance(config)

}

export default request