import request from "./request";

export function getdetail(iid){
    return request({
        url:'/detail',
        params:{
            iid
        }
    })
}

export function getRecommend(){
    return request({
        url:'/Recommend',
        // params:{
        //     item_id
        // }
    })
}


export class Goods {
    constructor(itemInfo,columns,services){
        this.title=itemInfo.title
        this.desc=itemInfo.desc
        this.highPrice=itemInfo.highPrice
        this.oldPrice=itemInfo.oldPrice
        this.discount=itemInfo.discountDesc
        this.columns=columns
        this.services=services
        this.realPrice=itemInfo.lowNowPrice 
    }
}


export class Shop {
    constructor(shopInfo){
        this.logo=shopInfo.shopLogo
        this.name=shopInfo.name
        this.fans=shopInfo.cFans
        
        this.sells=shopInfo.cSells
        // 评分
        this.score=shopInfo.score
        // 货物数量
        this.goodscount=shopInfo.cGoods
    }
}

// 详情描述
export class ShopDesc {
    constructor(detailInfo){
        this.Desc=detailInfo.desc
        this.image=detailInfo.detailImage
        this.keys=detailInfo.detailImage[0].key
    }
}

// 商品详细
export class Shopsize{
    constructor(itemParams){
        this.size=itemParams.rule.tables[0]
        this.set=itemParams.info.set
        this.name=itemParams.info.set[0].key
        this.value=itemParams.info.set[0].value
    }
}

// 评论信息
export class Comment {
    constructor(rate){
        // 评论数量
        this.crate=rate.cRate
        // 评论循环数组 
        this.list= rate.list
        // 评论主体
        this.content=rate.list[0].content
        // 商家回复
        this.explain=rate.list[0].explain
        this.style=rate.list[0].style
        // 评论主体id
        this.id=rate.list[0].rateId
        // 用户信息
        this.user=rate.list[0].user
        // userimage
        this.userimage=rate.list[0].user.avatar
        // 用户名字
        this.username=rate.list[0].user.name
        // 评详细
        this.extraInfo=rate.list[0].extraInfo
        this.image=rate.list[0].images
    }
}